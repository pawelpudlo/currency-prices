package com.example.price.model;

import java.util.List;

public class FormatJson {

    private String table;
    private String no;
    private String effectiveDate;
    private List<CurrencyJson> rates;

    public FormatJson(String table, String no, String effectiveDate, List rates) {
        this.table = table;
        this.no = no;
        this.effectiveDate = effectiveDate;
        this.rates = rates;
    }

    public String getTable() {
        return table;
    }

    public String getNo() {
        return no;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public List<CurrencyJson> getRates() {
        return rates;
    }
}
