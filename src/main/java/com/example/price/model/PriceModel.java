package com.example.price.model;

import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface PriceModel {

    @GET("/api/exchangerates/tables/A/?format=json")
    Call<List<FormatJson>> getPrice();

}
