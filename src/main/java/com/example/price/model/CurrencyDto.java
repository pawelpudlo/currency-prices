package com.example.price.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
public class CurrencyDto {

    private String date;
    private String currencyName;
    private String shortName;
    private double course;

    public CurrencyDto(String date, String currencyName, String shortName, double course) {
        this.date = date;
        this.currencyName = currencyName;
        this.shortName = shortName;
        this.course = course;
    }
}
