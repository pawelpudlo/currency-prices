package com.example.price.rest;

import com.example.price.model.CurrencyDto;
import com.example.price.service.PriceService;
import com.example.price.service.impl.PriceServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("api/")
public class PriceBankController {

    private final PriceService priceService;

    @Autowired
    public PriceBankController(PriceService priceService) {
        this.priceService = priceService;
    }

    @GetMapping("getPrice/{value}")
    public ResponseEntity<CurrencyDto> getPrice(@PathVariable(value = "value") String value ) throws IOException {

        return new ResponseEntity<>(this.priceService.getInformation(value),HttpStatus.OK);
    }

}
