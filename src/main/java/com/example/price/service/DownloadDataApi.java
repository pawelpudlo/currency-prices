package com.example.price.service;

import com.example.price.model.CurrencyDto;

import java.io.IOException;

public interface DownloadDataApi {

    void downloadData() throws IOException;

    CurrencyDto getCurrencyInformation(String shortNameValue);

}
