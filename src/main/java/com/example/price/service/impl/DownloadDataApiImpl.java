package com.example.price.service.impl;

import com.example.price.model.CurrencyDto;
import com.example.price.model.CurrencyJson;
import com.example.price.model.FormatJson;
import com.example.price.model.PriceModel;
import com.example.price.service.DownloadDataApi;
import com.example.price.service.RetrofitClient;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DownloadDataApiImpl implements DownloadDataApi {

    private final List<CurrencyDto> currencyDataList;

    public DownloadDataApiImpl() throws IOException {
        this.currencyDataList = new ArrayList<CurrencyDto>();
        downloadData();
    }

    @Override
    public void downloadData() throws IOException {

        try {
            RetrofitClient retrofitClient = new RetrofitClient();

            PriceModel priceModel = retrofitClient.getRetrofitClient().create(PriceModel.class);
            Response<List<FormatJson>> response =
                    priceModel.getPrice().execute();
            //List<FormatJson> allCurrencyInformation = response.body();
            assert response.body() != null;
            String date = response.body().get(0).getEffectiveDate();
            for(CurrencyJson dataCurrency: response.body().get(0).getRates()){
                this.currencyDataList.add(new CurrencyDto(date,dataCurrency.getCurrency(),dataCurrency.getCode(),dataCurrency.getMid()));

            }

        }catch (IOException io){
            System.out.println(io.getMessage());
            System.out.println("1");
        }
    }

    @Override
    public CurrencyDto getCurrencyInformation(String shortNameValue) {

        for(CurrencyDto currencyData:this.currencyDataList){
            if(currencyData.getShortName().equals(shortNameValue)){
                return currencyData;
            }

        }
        return null;
    }
}
