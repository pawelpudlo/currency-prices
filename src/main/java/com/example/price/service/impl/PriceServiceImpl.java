package com.example.price.service.impl;

import com.example.price.model.CurrencyDto;
import com.example.price.service.DownloadDataApi;
import com.example.price.service.PriceService;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
public class PriceServiceImpl implements PriceService {

    @Override
    public CurrencyDto getInformation(String code) throws IOException {
/*
        try {

        }catch (NullPointerException nullPointerException){
            System.out.println(nullPointerException.getMessage());
            System.out.println("2");
            return null;
        }


 */
        DownloadDataApi downloadDataApi = new DownloadDataApiImpl();
        return downloadDataApi.getCurrencyInformation(code);
    }
}
