package com.example.price.service;

import com.example.price.model.CurrencyDto;

import java.io.IOException;

public interface PriceService {

        CurrencyDto getInformation(String code) throws IOException;

}
