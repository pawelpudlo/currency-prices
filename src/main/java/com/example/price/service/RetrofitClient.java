package com.example.price.service;

import okhttp3.OkHttpClient;
import org.springframework.stereotype.Component;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Component
public class RetrofitClient {

    public RetrofitClient() {
    }

    public Retrofit getRetrofitClient(){
        OkHttpClient okHttpClient = new OkHttpClient();
        return new Retrofit.Builder()
                .baseUrl("http://api.nbp.pl/api/exchangerates/tables/A/?format=json")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

}
